#!/usr/bin/env node

import yargs from 'yargs/yargs'
import { hideBin } from 'yargs/helpers'
// Commands
import { create } from '../lib/create.js'

const argv = yargs(hideBin(process.argv))
  .scriptName('')
  .command('create', 'Creates a new project using the specified template', () => {}, create)
  .option('template', {
    alias: 't',
    type: 'string',
    demandOption: true,
    describe: 'Template for the project'
  })
  .option('path', {
    alias: 'p',
    type: 'string',
    default: '',
    describe: 'The path where the project should be started'
  })
  .usage('Usage: tzso create --template <template-name> --path <folder-path>')
  .parse()
