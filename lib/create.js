import URL from 'url'
import path from 'path'
import download from '@serverless/utils/download.js'

const TEMPLATES_REPO_URL = 'https://gitlab.com/MatiasTorelli/templates'

const create = async ({ template, path }) => {
  try {
    await downloadTemplate(template, path)
    console.log('Template created successfully')
  } catch (error) {
    console.log(`Couldn't download the template: ${template}`)
  }
}

const downloadTemplate = async (template, path) => {
  const downloadOptions = {
    timeout: 30000,
    extract: true,
    strip: 1,
    mode: '755'
  }
  const parsedUrl = URL.parse(TEMPLATES_REPO_URL.replace(/\/$/, ''))
  const downloadUrl = getDownloadUrl(parsedUrl, template)

  return await download(downloadUrl, `./${path}`, downloadOptions)
}

const getDownloadUrl = (url, template) => {
  const pathLength = 4
  const parts = url.pathname.split('/')
  const isSubdirectory = parts.length > pathLength
  const owner = parts[1]
  const repo = parts[2]
  const branch = 'main'
  let directoryPath = undefined
  
  if (isSubdirectory) {
    directoryPath = getDirectoryPath(pathLength + 1, parts)
    const directoriesList = directoryPath.split('\\')
    directoryPath = directoriesList.splice(1, directoriesList.length).join('/')
    return `https://gitlab.com/${owner}/${repo}/-/archive/${branch}/${repo}-${branch}.zip?path=${directoryPath}/${template}`
  } else {
    return `https://gitlab.com/${owner}/${repo}/-/archive/${branch}/${repo}-${branch}.zip?path=${template}`
  }
}

const getDirectoryPath = (length, parts) => {
  if (!parts) {
    return ''
  }
  return parts.slice(length).filter(Boolean).join(path.sep)
}

export { create }
